"use strict";
const Q = require('q');
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const fs = require('fs');

Q.async(function* () {
    let config = {};

    try {
        config.port = yield Q.Promise((resolve, reject) => {
            rl.question("What port is the server listening on?\n", resp => resolve(resp));
        });

        config.dbURI = yield Q.Promise((resolve, reject) => {
            rl.question(
                "Where is the database located?\n" +
                "i.e.:mongodb://localhost/example\n",
                resp => resolve(resp));
        });

        fs.writeFile(__dirname + "/config.json", JSON.stringify(config), err => {
            if (err) {
                return console.error(err);
            }
            console.info("Wrote configuration to:\t" + __dirname + "/config.json");
            rl.close();
        });
    }
    catch (e) {
        console.log("Unable to save configuration");
        console.error(e);
    }
})();