# Econovizer

With everything becoming so interconnected it's hard to see how your information is kept private.
Econovizer is a self-managed solution for those who want to keep track of their finances without
having to feel threatened by the cloud. With Econovizer you can self-host the application on
your local network without having to reach out to the internet.

**Disclaimer:** This application is still being worked on and may be buggy.

## Installation

Make sure that [NodeJS](https://nodejs.org) and [MongoDB](https://mongodb.org) are installed on your respective
platform.
After Node and Mongo are installed make sure that the mongo daemon is running (mongod, if you're on Linux, Mongo
installs as a service).
Run the following command to build the sources and download all of the dependencies

```bash
npm install
```

This command will present some prompts for the initial configuration. You'll be asked for the port number and the 
database URI.

### Starting the program

```bash
npm start
# or
node server/app.js
```

or you could use a system like [nodemon](http://nodemon.io/) or [pm2](https://github.com/Unitech/pm2) to monitor the
node process and treat it like a service.