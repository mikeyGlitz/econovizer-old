"use strict";
const client = require('./paths.json').client;
const gulp = require('gulp');

gulp.task('assets', () => {
    return gulp.src(client.assets)
        .pipe(gulp.dest(client.build));
});