"use strict";
const client = require('./paths.json').client;
const gulp = require('gulp');
const sass = require('gulp-sass');
const gutil = require('gulp-util');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', () => {
    if (gutil.env.production) {
        return gulp.src(client.sass)
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest(client.build));
    }
    return gulp.src(client.sass)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(client.build));
});