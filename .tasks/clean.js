"use strict";
const dist = require('./paths.json').client.build;
const del = require('del');
const gulp = require('gulp');
const gutil = require('gulp-util');

gulp.task('clean', () => {
    return del(dist).then((paths) => gutil.log("Deleted paths:\n", paths.join('\n')));
});