"use strict";
const gulp = require('gulp');
const gutil = require('gulp-util');
const tests = require('./paths').tests;
const tester = require('gulp-mocha');

gulp.task('test', () => {
    return gulp.src(tests, { read: false })
        .pipe(tester({ reporter: 'list' }))
        .on('error', gutil.log);
});