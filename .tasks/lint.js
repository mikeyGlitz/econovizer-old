"use strict";
const sources = require('./paths.json').sources;
const scripts = require('./paths.json').client.scripts;
const gulp = require('gulp');
const jshint = require('gulp-jshint');
const tslint = require('gulp-tslint');
const gutil = require('gulp-util');

gulp.task('jshint', () => {
    return gulp.src(sources)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('tslint', () => {
    return gulp.src(scripts)
        .pipe(tslint())
        .pipe(tslint.report('verbose'));
})