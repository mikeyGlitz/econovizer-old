// This script depends on other scripts like script.js, assets.js, and sass.js
"use strict";
const paths = require('./paths.json');
const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const livereload = require('gulp-livereload');

gulp.task('watch', () => {
    gulp.watch(paths.client.assets, ['assets']);
    gulp.watch(paths.client.scripts, ['scripts']);
    gulp.watch(paths.client.jade, ['jade'])
    gulp.watch(paths.client.sass, ['sass']);
    gulp.watch(paths.sources, ['jshint']);
});

gulp.task('serve', ['build'], () => {
    return nodemon({
        script: 'server/app.js',
        option: '-i client/*'
    });
});

gulp.task('livereload', ['serve'], () => {
    gulp.watch(paths.client.assets, ['assets']);
    gulp.watch(paths.client.sass, ['sass']);
    gulp.watch(paths.client.jade, ['jade']);
    gulp.watch(paths.client.scripts, ['scripts']);
    
    let server = livreload();
    let all_build_files = paths.client.build + '/**/*';
    return gulp.watch(all_build_files, (evt) => {
        server.changed(evt.path);
    });
});