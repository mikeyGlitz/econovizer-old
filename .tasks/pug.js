"use strict";
const client = require('./paths.json').client;
const gulp = require('gulp');
const gutil = require('gulp-util');
const pug = require('gulp-pug');

// Due to legal disputes, jade has been renamed to pug
gulp.task('pug', () => {
    return gulp.src(client.jade)
        .pipe(pug({ pretty: true }))
        .pipe(gulp.dest(client.build));
});