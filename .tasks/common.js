"use strict";
const gulp = require('gulp');

gulp.task('default', ['build']);
gulp.task('build', ['scripts', 'pug', 'sass', 'assets']);
gulp.task('develope', ['build', 'serve', 'livereload']);
gulp.task('debug', ['jshint', 'tslint', 'test', 'build', 'serve', 'watch']);