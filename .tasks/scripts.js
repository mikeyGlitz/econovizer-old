"use strict";
const cPaths = require("./paths.json").client;
const gulp = require('gulp');
const gutil = require('gulp-util')
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const browserify = require('browserify');



gulp.task('scripts', ['tslint'], () => {
    const b = browserify({
        entries: ['./client/script/app.ts'],
        extensions: ['.js', '.json', '.ts'],
        debug: !gutil.env.production,
    });

    let tsOpts = {
        sourcemaps: !gutil.env.production,
        typescript: require('typescript')
    };

    return b
        .plugin("tsify", tsOpts)
        .transform('babelify', { presets: ["es2015"], extensions: ['.ts', '.js'] })
        .bundle()
        .pipe(source('js/app.js'))
        .pipe(buffer())
        .pipe(gulp.dest(cPaths.build));
});