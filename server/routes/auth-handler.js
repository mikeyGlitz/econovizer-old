"use strict";
const logger = require('winston');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../model/user');
const mongoose = require('mongoose');
const ErrReporter = require('../errReporter');

const login = (email, password, done) => {
    logger.info(JSON.stringify({ email: email, password: password }));
    User.findOne({ 'email': email, 'password': password }, (err, user) => {
        logger.info("Received: " + JSON.stringify(user));
        if (err) { return done(err); }
        return done(null, user);
    });
};

const createUserAndLogin = (req, resp) => {
    let body = req.body;
    logger.info("Received request with body:\n" + JSON.stringify(body));

    let user = new User(body);
    user.save((err, user) => {
        if (err) {
            logger.error(err);
            let reporter = new ErrReporter(err);
            if (err.code === 111000) {
                return resp.status(400).send(reporter.errorToJSON());
            }
            return resp.status(500).send(reporter.errorToJSON());
        }
        req.logIn(user, err => {
            if (err) {
                logger.error(err);
                let reporter = new ErrReporter(err);
                return resp.status(500).send(reporter.errorToJSON());
            }

            //We just want to return the essential User information
            let body = {
                uid: user.id
            };
            logger.info("Sending: " + JSON.stringify(body));
            return resp.status(200).json(body);
        });
    });
};

// Function to make sure the API is authenticated
const ensureAuthenticated = (req, resp, next) => {
    logger.log(req.isAuthenticated());
    if (req.isAuthenticated()) {
        return next();
    }
    else if (!mongoose.connection.readyState) {
        let reporter = new ErrReporter(new Error('Authentication unavailable'));
        resp.status(503).send(reporter.errorToJSON());
    }
    else {
        let reporter = new ErrReporter(new Error('Not authorized'));
        logger.error(reporter.getError());
        resp.status(401).send(reporter.errorToJSON());

    }
};

const init = app => {
    // Initialize the session store
    const session = require('express-session');
    const MongoStore = require('connect-mongo')(session);

    app.use(session({
        secret: 'unguessable',
        resave: true,
        saveUninitialized: false,
        store: new MongoStore({
            mongooseConnection: mongoose.connection
        })
    }));

    // Initialize the express middleware
    app.use(passport.initialize());
    app.use(passport.session());

    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
            if (err) {
                return done(err);
            }
            return done(null, user);
        });
    });

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.use(new LocalStrategy({
        usernameField: 'email'
    }, login));

    app.post('/api/users/login', (req, resp, next) => {
        passport.authenticate('local', (err, user, info) => {
            if (err) {
                logger.error(err);
                let reporter = new ErrReporter(err);
                resp.status(500).send(reporter.errorToJSON());
                return next(err);
            }
            if (user) {
                req.logIn(user, (err) => {
                    if(err){
                        let reporter = new ErrReporter(err);
                        resp.status(500).send(reporter.errorToJSON());
                        next(err);
                    }
                    return resp.status(200).json({ success: true, uid: user.id });
                });
            }
            else {
                let reporter = new ErrReporter(new Error('Unable to log in user'));
                logger.error(reporter.getError());
                resp.status(400).send(reporter.errorToJSON());
                return next(err);
            }
        })(req, resp, next);
    });

    // Logout action
    app.get('/api/users/logout', (req, resp) => {
        req.logout();
        resp.redirect("/");
    });

    // Route to determine whether the current user is logged in or not
    app.get('/api/users/loggedin', (req, resp) => {
        logger.info(req.isAuthenticated());
        if (req.isAuthenticated()) {
            if (req.user) {
                return resp.status(200).json({ success: true, uid: req.user.id });
            }
            else {
                let reporter = new ErrReporter(new Error('Malformed Request'));
                return resp.status(400).send(reporter.errorToJSON());
            }
        }
        else {
            let reporter = new ErrReporter(new Error('Not authorized'));
            return resp.status(403).send(reporter.errorToJSON());
        }
    });

    app.post('/api/users', createUserAndLogin);
};

module.exports = {
    init: init,
    ensureAuthenticated: ensureAuthenticated
};