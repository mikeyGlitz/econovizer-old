'use strict';
const Account = require('../model/account');
const User = require('../model/user');
const Transaction = require('../model/transaction');
const logger = require('winston');
const authed = require('./auth-handler').ensureAuthenticated;
const ErrReporter = require('../errReporter');
const co = require('co');

function handleError(err, resp) {
    logger.error(err);
    let reporter = new ErrReporter(err);
    return resp.status(500).send(reporter.errorToJSON());
}

function getAccountsCB(req, resp) {
    let uid = req.params.uid;
    User.findOne({ '_id': uid })
        .populate('accounts')
        .exec((err, user) => {
            if (err) {
                return handleError(err, resp);
            }
            let accounts = user.accounts;
            return resp.status(200).json(accounts);
        });
}

function getAccountCB(req, resp) {
    let uid = req.params.uid;
    let acctId = req.params.acctId;

    Account.findById(acctId, (err, account) => {
        if (err) {
            return handleError(err, resp);
        }
        if (account._user !== uid) {
            return handleError(new Error('User doesn\'t match with account'), resp);
        }
        resp.status(200).json(account);
    });
}

function* addAccount(req, resp) {
    let uid = req.params.uid;
    let account = new Account(req.body);
    account._user = uid;

    try {
        account = yield account.save();

        let user = yield User.findById(uid).exec();
        user.accounts.push(account);
        yield user.save();
    } catch (error) {
        return handleError(error, resp);
    }

    return resp.status(200).json(account);
}

function* updateAccount(req, resp) {
    let uid = req.params.uid;
    let aid = req.params.acctId;
    let acct = req.body;

    let account;
    try {
        account = yield Account.findById(aid).exec();
        if (acct.name) {
            account.name = acct.name;
        }
        if (acct.openingBalance) {
            account.openingBalance = acct.openingBalance;
        }
        if (acct.type) {
            account.type = acct.type;
        }

        account = yield account.save();
    }
    catch (error) {
        return handleError(error, resp);
    }
    return resp.status(200).json(account);
}

function* deleteAccount(req, resp) {
    let aid = req.params.acctId;
    let account;
    try {
        // account = yield Account.findById(aid).populate('_user').exec();

        // let user = account._user;
        // let accountIndex = user.accounts.indexOf(account);
        // user.accounts.splice(accountIndex, 1);
        // yield user.save();

        let account = yield Account.findById(aid).exec();
        yield account.remove();
    } catch (error) {
        handleError(error, resp);
    }

    return resp.status(200).json({ aid: aid });
}

function* getBalance(req, resp) {
    let aid = req.params.acctId;
    let account;
    try {
        account = yield Account.findById(aid).populate({
            path: 'transactions',
            populate: { path: 'type' }
        }).exec();

        let balance = account.openingBalance;
        let transactions = account.transactions;
        for (let transaction of transactions) {
            let type = transaction.type.type;
            if (type === 'income') {
                balance += transaction.amount;
            }
            else {
                balance -= transaction.amount;
            }
        }

        resp.status(200).json({ balance: balance });
    }
    catch (error) {
        handleError(error, resp);
    }
}

module.exports = app => {
    //Account routes
    app.get('/api/users/:uid/accounts', authed, getAccountsCB);
    app.get('/api/users/:uid/accounts/:acctId', authed, getAccountCB);
    app.post('/api/users/:uid/accounts', authed, (req, resp) => co(addAccount(req, resp)));
    app.put('/api/users/:uid/accounts/:acctId', authed, (req, resp) => co(updateAccount(req, resp)));
    app.delete('/api/users/:uid/accounts/:acctId', authed, (req, resp) => co(deleteAccount(req, resp)));
    app.get('/api/users/:uid/accounts/:acctId/balance', authed, (req, resp) => co(getBalance(req, resp)));
};