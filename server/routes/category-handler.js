'use strict';
const Category = require('../model/category');
const User = require('../model/user');
const logger = require('winston');
const co = require('co');
const ErrReporter = require('../errReporter');

function reportError(err, resp) {
    let reporter = new ErrReporter(err);
    return resp.status(500).send(reporter.errorToJSON());
}

function getCategories(req, resp) {
    let uid = req.params.uid;

    User.findById(uid).populate('categories').exec((err, user) => {
        if (err) {
            return reportError(err, resp);
        }
        return resp.status(200).json(user.categories);
    });
}

function* addCategory(req, resp) {
    let body = req.body;
    let category = new Category(body);
    let uid = req.params.uid;
    category._user = uid;

    try {
        // Save the category and update its reference so we have the _id field
        category = yield category.save();

        // Get the user object and populate its categories field.
        // Update the user object
        let user = yield User.findById(uid).exec();
        logger.info(user);
        user.categories.push(category);
        yield user.save();
    } catch (error) {
        return reportError(error, resp);
    }
    return resp.status(200).json(category);
}

function* updateCategory(req, resp) {
    let body = req.body;
    let cid = req.params.cid;

    let category;
    try {
        category = yield Category.findById(cid).exec();
        if (body.name) {
            category.name = body.name;
        }
        if (body.type) {
            category.type = body.type;
        }
        category = yield category.save();
    } catch (error) {
        return reportError(error, resp);
    }

    return resp.status(200).json(category);
}

function* deleteCategory(req, resp) {
    let cid = req.params.cid;

    try {
        let category = yield Category.findById(cid);
        yield category.remove();
    } catch (error) {
        return reportError(error, resp);
    }
    return resp.status(200).json({ cid: cid });
}

module.exports = app => {
    app.get('/api/users/:uid/categories', (req, resp) => co(getCategories(req, resp)));
    app.post('/api/users/:uid/categories', (req, resp) => co(addCategory(req, resp)));
    app.put('/api/users/:uid/categories/:cid', (req, resp) => co(updateCategory(req, resp)));
    app.delete('/api/users/:uid/categories/:cid', (req, resp) => co(deleteCategory(req, resp)));
};