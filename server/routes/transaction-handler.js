'use strict';
const co = require('co');
const logger = require('winston');
const Transaction = require('../model/transaction');
const ErrReporter = require('../errReporter');
const Account = require('../model/account');
const authed = require('./auth-handler').ensureAuthenticated;

function handleError(err, resp) {
    logger.error(err);
    let reporter = new ErrReporter(err);
    return resp.status(500).send(reporter.errorToJSON());
}

function* getTransactions(req, resp) {
    let aid = req.params.acctId;

    try {
        let account = yield Account.findById(aid).populate({
            path: 'transactions',
            populate: { path: 'type' }
        }).exec();

        let transactions = account.transactions;

        return resp.status(200).json(transactions);
    } catch (error) {
        return handleError(error);
    }
}

function* addTransaction(req, resp) {
    try {
        let body = req.body;
        logger.info(body);
        let transaction = new Transaction(body);

        let aid = req.params.acctId;
        transaction._account = aid;

        transaction = yield transaction.save();
        logger.info(transaction);

        // Update the account with the new transaction

        logger.info(aid);
        let account = yield Account.findById(aid).populate('transactions').exec();
        logger.info(account);
        account.transactions.push(transaction);
        yield account.save();

        // Populate the transaction
        transaction = yield Transaction.findById(transaction._id).populate('type').exec();
        logger.info(transaction);

        resp.status(200).json(transaction);
    } catch (error) {
        return handleError(error, resp);
    }
}

function* updateTransaction(req, resp) {
    let tid = req.params.tid;
    let transaction;
    try {
        transaction = yield Transaction.findById(tid).exec();

        let reqbody = req.body;
        if (reqbody.recipient) {
            transaction.recipient = reqbody.recipient;
        }
        if (reqbody.amount) {
            transaction.amount = reqbody.amount;
        }
        if (reqbody.date) {
            transaction.date = reqbody.date;
        }
        if (reqbody.notes) {
            transaction.notes = reqbody.notes;
        }
        if (reqbody.type) {
            transaction.type = reqbody.type;
        }
        yield transaction.save();

        transaction = yield transaction.populate('type').execPopulate();
    } catch (error) {
        handleError(error, resp);
    }

    return resp.status(200).json(transaction);
}

function* removeTransaction(req, resp) {
    let tid = req.params.tid;

    try {
        let transaction = yield Transaction.findById(tid).exec();
        yield transaction.remove();
    } catch (error) {
        return handleError(error, resp);
    }

    return resp.status(200).json({ tid: tid });
}

module.exports = function (app) {
    app.get('/api/users/:uid/accounts/:acctId/transactions', authed, (req, resp) => co(getTransactions(req, resp)));
    app.post('/api/users/:uid/accounts/:acctId/transactions', authed, (req, resp) => co(addTransaction(req, resp)));
    app.put('/api/users/:uid/accounts/:acctId/transactions/:tid', authed, (req, resp) => co(updateTransaction(req, resp)));
    app.delete('/api/users/:uid/accounts/:acctId/transactions/:tid', authed, (req, resp) => co(removeTransaction(req, resp)));
};