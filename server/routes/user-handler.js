'use strict';
const logger = require('winston');
const User = require('../model/user');
const Account = require('../model/account');
const Transaction = require('../model/transaction');
const authed = require('./auth-handler').ensureAuthenticated;
const ErrReporter = require('../errReporter');
const co = require('co');
// Convenience class to make rendering User objects to the client-side easier
// Note the password field is missing. We do not want to expose that to the client-side
class RespUser {
    constructor(user) {
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.email = user.email;
        this._id = user._id;
    }

    toJSON() {
        let jsonObj = {
            firstName: this.firstName,
            lastName: this.lastName,
            email: this.email,
            _id: this._id
        };

        return JSON.stringify(jsonObj);
    }
}

function handleError(err, resp) {
    logger.error(err);
    let reporter = new ErrReporter(err);
    return resp.status(500).send(reporter.errorToJSON());
}

function getUser(req, resp) {
    let uid = req.params.uid;
    User.findById(uid, (err, user) => {
        if (err) {
            return handleError(err, resp);
        }
        let usr = new RespUser(user);
        return resp.status(200).send(usr.toJSON());
    });
}

function updateUser(req, resp) {
    let uid = req.params.uid;
    let reqUser = req.body;
    User.findById(uid, (err, user) => {
        if (err) {
            handleError(err, resp);
        }
        if (reqUser.firstName)
            user.firstName = reqUser.firstName;
        if (reqUser.lastName)
            user.lastName = reqUser.lastName;
        if (reqUser.password)
            user.password = reqUser.password;

        user.save((err, user) => {
            if (err) {
                return handleError(err, resp);
            }
            let usr = new RespUser(user);
            return resp.status(200).send(usr.toJSON());
        });
    });
}

function* deleteUser(req, resp) {
    let uid = req.params.uid;

    try {
        let user = yield User.findById(uid).exec();
        yield user.remove();
    } catch (error) {
        return handleError(error, resp);
    }

    return resp.status(200).json({ uid: uid });
}

module.exports = app => {
    app.get('/api/users/:uid', authed, getUser);
    app.put('/api/users/:uid', authed, updateUser);
    app.delete('/api/users/:uid', authed, (req, resp) => co(deleteUser(req, resp)));
};