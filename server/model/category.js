'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SchemaTypes = Schema.Types;
const logger = require('winston');
let co = require('co');

let categorySchema = new Schema({
    name: String,
    type: { type: String, enum: ['income', 'expense'] },
    _user: { type: SchemaTypes.ObjectId, ref: 'User' }
});


function* removeSubDocs(doc, next) {
    try {
        let transactions = yield mongoose.models.Transaction.find({ type: doc._id }).exec();
        logger.info("=== Transactions ===");
        logger.info(transactions);

        for (let transaction of transactions) {
            transaction.type = undefined;
            transaction.save();
        }
    } catch (error) {
        return next(error);
    }
    return next();
}

categorySchema.post('remove', (doc, next) => co(removeSubDocs(doc, next)));

module.exports = mongoose.model('Category', categorySchema);