'use strict';
const mongoose = require('mongoose');
// Enrich the mongoose object with the double type
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;
const logger = require('winston');

let transactionSchema = new Schema({
    recipient: String,
    notes: String,
    date: Date,
    type: { type: SchemaTypes.ObjectId, ref: 'Category' },
    amount: SchemaTypes.Double,
    _account: { type: SchemaTypes.ObjectId, ref: 'Account' }
});

transactionSchema.post('remove', (doc, next) => {
    logger.info("Transaction Post remove fired");
    logger.info(doc);

    mongoose.models.Account.findById(doc._account).populate('transactions').exec((err, account) => {
        if (err) {
            logger.error(err);
            return next(err);
        }
        if (account) {
            logger.info("=== Account ===");
            logger.info(account);
            account.transactions.pull({ _id: doc._id });
            account.save();
        }
        next();
    });
});

module.exports = mongoose.model('Transaction', transactionSchema);