'use strict';
const mongoose = require('mongoose');
// Enrich the mongoose object with the double type
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;
const logger = require('winston');
const co = require('co');

let accountSchema = new Schema({
    openingBalance: SchemaTypes.Double,
    name: String,
    type: String,
    _user: { type: SchemaTypes.ObjectId, ref: 'User' },
    transactions: [{ type: SchemaTypes.ObjectId, ref: 'Transaction' }]
});

function* accountSubRemoval(doc, next) {
    try {
        let transactions = yield mongoose.models.Transaction.find({ '_account': doc._id }).exec();
        for (let transaction of transactions) {
            yield transaction.remove();
        }

        let user = yield mongoose.models.User.findById(doc._user).populate('accounts').exec();
        if (user) {
            user.accounts.pull({ '_id': doc._id });

            yield user.save();
        }
    } catch (error) {
        logger.error(error);
        return next(error);
    }
    return next();
}

accountSchema.post('remove', (doc, next) => co(accountSubRemoval(doc, next)));

module.exports = mongoose.model('Account', accountSchema);