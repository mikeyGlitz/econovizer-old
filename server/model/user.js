"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const logger = require('winston');
const co = require('co');

let userSchema = new Schema({
    firstName: String,
    lastName: String,
    password: String,
    email: { type: String, required: true, unique: true },
    accounts: [{ type: Schema.Types.ObjectId, ref: 'Account' }],
    categories: [{ type: Schema.Types.ObjectId, ref: 'Category' }]
});

function* removeUserSubs(doc, next) {
    logger.info('==== Document: ====');
    logger.info(doc);
    try {
        let categories = yield mongoose.models.Category.find({ _user: doc._id });
        logger.info(categories.length);
        for (let category of categories) {
            yield category.remove();
        }

        let accounts = yield mongoose.models.Account.find({ _user: doc._id }).exec();
        logger.info(accounts.length);
        for (let account of accounts) {
            yield account.remove();
        }
    } catch (error) {
        logger.error(error);
        return next(error);
    }
    return next();
}

userSchema.post('remove', (doc, next) => co(removeUserSubs(doc, next)));

module.exports = mongoose.model('User', userSchema);