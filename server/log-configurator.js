"use strict";
const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const fs = require('fs');
const dateFormat = require('dateformat');

if (!fs.existsSync('log')) {
    fs.mkdirSync('log');
}

function getTimestamp() {
    return dateFormat((new Date()), 'isoDateTime');
}

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, { colorize: true, timestamp: getTimestamp });

winston.add(DailyRotateFile, {
    filename: 'log/genlog.log',
    datePattern: '.yyyy-MM-dd',
    timestamp: getTimestamp
});