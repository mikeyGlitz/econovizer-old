"use strict";
const app = require('express')();
const serve = require('express-static');
const morgan = require('morgan');

// Set up general logging
require('./log-configurator.js');
const logger = require('winston');

// Set up morgan for HTTP access logs
const FileStreamRotator = require('file-stream-rotator');
let accessLogStream = FileStreamRotator.getStream({
    date_format: 'YYYYMMDD',
    filename: 'log/access-%DATE%.log',
    frequency: 'daily',
    verbose: false
});
app.use(morgan('combined', { stream: accessLogStream }));

// Set up parsing for sessions
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(require('cookie-parser')());

// Set up the user routes
const init = require('./routes/auth-handler').init;
init(app);
require('./routes/user-handler')(app);
require('./routes/account-handler')(app);
require('./routes/transaction-handler')(app);
require('./routes/category-handler')(app);

// Set up database connection
require('./db-configurator');

app.use('/lib', serve(__dirname + '/../node_modules'));
app.use('/lib', serve(__dirname + '/../bower_components'));
app.use(serve(__dirname + '/../dist'));

const config = require('../config.json');
let port = config.port;

let server = app.listen(port, () => {
    let port = server.address().port;
    let address = server.address().address;
    logger.info(`Listening on: ${address}:${port}...`);
});