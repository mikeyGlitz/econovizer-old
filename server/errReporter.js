'use strict';

module.exports = class {

    constructor(err) {
        this.error = err;
    }
    
    getError(){
        return this.error;
    }

    errorToJSON() {
        let errObj = { success: false };
        errObj.message = this.error.message || this.error.errmsg;

        return JSON.stringify(errObj);
    }
};