"use strict";
const mongoose = require('mongoose');
const logger = require('winston');
const config = require('../config.json');

const dbURI = config.dbURI;

mongoose.connect(dbURI);

mongoose.connection.on('error', err => logger.error("Connection error: " + err));
mongoose.connection.on("connected", () => logger.info("Connected to db at: " + dbURI));
mongoose.connection.on('disconnected', () => logger.info("Disconnected from " + dbURI));

process.on("SIGINT", () => {
    mongoose.connection.close(()=>{
        logger.info("Connection to " + dbURI + " disconnected through app termination");
        process.exit(0);
    });
});