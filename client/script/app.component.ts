// Baked in Angular modules
import {Component} from "angular2/core";
import {RouteConfig, ROUTER_DIRECTIVES} from "angular2/router";
import { HTTP_PROVIDERS } from 'angular2/http';

import { ProtectedDirective } from './directives/protected.directive';

// Provider modules
import { UserService, AuthService } from './services/user.service';

// Component modules
import {LoginComponent} from "./components/login.component";
import {DashComponent} from "./components/dash.component";

@Component({
    selector: "app",
    template: `<router-outlet></router-outlet>`,
    directives: [ROUTER_DIRECTIVES, LoginComponent, ProtectedDirective],
    providers: [HTTP_PROVIDERS, UserService, AuthService]
})

@RouteConfig([
    {
        path: "/sign-on",
        name: "SignOn",
        component: LoginComponent,
        useAsDefault: true
    },
    {
        path: "/dash",
        name: "Dash",
        component: DashComponent
    }
])

export class AppComponent { }