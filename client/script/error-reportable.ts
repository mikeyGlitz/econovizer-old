import { Observable } from 'rxjs/Rx';

export class ErrorReportable {
    reportError(error: any, message?: String) {
        console.error(error);
        return Observable.throw(error.json() || message);
    }
}