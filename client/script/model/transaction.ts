/// <reference path="../../../typings/main/definitions/moment/index.d.ts" />
/// <reference path="../../../typings/main/ambient/accounting/index.d.ts" />

import * as moment from 'moment';
import * as accounting from 'accounting';
import { Category } from './category';

export class Transaction {
    recipient: String;
    amount: Number;
    date: Date;
    notes: String;
    type: Category;
    _id: String;

    constructor(other?: Transaction) {
        if (other) {
            this.recipient = other.recipient;

            let convertedNumber = accounting.toFixed(<number>other.amount, 2);
            this.amount = parseFloat(convertedNumber);

            this.date = moment(other.date).toDate();
            this.notes = other.notes;
            this.type = new Category(other.type);
            this._id = other._id;
        }
    }

    toJSON() {
        // let type = this.type;
        // let tid = type._id;
        let jsonObj: any = {
            recipient: this.recipient,
            amount: this.amount,
            type: this.type._id,
            date: this.date
        };

        if (this.notes) {
            jsonObj.notes = this.notes;
        }

        if (this._id) {
            jsonObj._id = this._id;
        }

        return JSON.stringify(jsonObj);
    }
}