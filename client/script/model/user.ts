export class User {
    firstName: String;
    lastName: String;
    email: String;
    password: String;
    _id: String;

    constructor(other?: User) {
        if (other) {
            this.firstName = other.firstName;
            this.lastName = other.lastName;
            this.email = other.email;
            this._id = other._id;
        }
    }

    toJSON() {
        let jsonObj: any = {
            email: this.email,
            password: this.password
        };

        // Add values if we're registering
        if (this.firstName) {
            jsonObj.firstName = this.firstName;
        }
        if (this.lastName) {
            jsonObj.lastName = this.lastName;
        }
        if (this._id) {
            jsonObj.id = this._id;
        }

        return JSON.stringify(jsonObj);
    }
}