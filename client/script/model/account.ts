import * as accounting from 'accounting';
export class Account {
    _id: String;
    name: String;
    _user: String;
    openingBalance: Number = 0;
    type: String;

    constructor(other?: Account) {
        if (other) {
            this._id = other._id;
            this.name = other.name;
            this._user = other._user;

            let convertedBalance = accounting.toFixed(<number>other.openingBalance, 2);
            this.openingBalance = parseFloat(convertedBalance);

            this.type = other.type;
        }
    }

    toJSON() {
        let jsonObj: any = {
            name: this.name,
            openingBalance: this.openingBalance,
            user: this._user
        };

        if (this._id) {
            jsonObj.id = this._id;
        }

        return JSON.stringify(jsonObj);
    }
}