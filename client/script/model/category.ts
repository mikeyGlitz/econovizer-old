export class Category {
    name: String;
    type: String;
    _id: String;

    constructor(other?: Category) {
        if (other) {
            this._id = other._id;
            this.name = other.name;
            this.type = other.type;
        }
    }

    toJSON() {
        let jsonObj: any = {
            name: this.name,
            type: this.type
        };
        if (this._id) {
            jsonObj.user = this._id;
        }
        return JSON.stringify(jsonObj);
    }
}