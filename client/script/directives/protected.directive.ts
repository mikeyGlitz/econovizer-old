import { Directive } from 'angular2/core';
import { AuthService } from '../services/user.service';
import { Router, Location } from 'angular2/router';
import { Response } from 'angular2/http';

@Directive({
    selector: '[protected]'
})

export class ProtectedDirective {
    constructor(
        private _authService: AuthService,
        private _location: Location,
        private _router: Router
    ) {
        this._authService.checkAuthenticated();
    }
}