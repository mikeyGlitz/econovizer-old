import {Component, Injectable, OnInit } from 'angular2/core';
import { FORM_DIRECTIVES, FormBuilder, ControlGroup, Validators } from 'angular2/common';
import { AlphaValidator, MatchingFieldValidator } from './validators';
import {User} from '../model/user';
import { AuthService, UserService } from '../services/user.service';
import { CategoryDialogComponent } from './category.dialog.component';

declare var jQuery: any;

@Component({
    templateUrl: '/view/user-panel.html',
    selector: 'user-panel',
    directives: [FORM_DIRECTIVES, CategoryDialogComponent]
})

@Injectable()
export class UserPanelComponent implements OnInit {
    currentUser: User;
    model: User = this._userService.createUser();
    errorMessage: String;
    userForm: ControlGroup;

    constructor(private _userService: UserService, private _authService: AuthService, private _formBuilder: FormBuilder) { }

    buildForm() {
        this.userForm = this._formBuilder.group({
            firstName: ['', Validators.compose([Validators.required, AlphaValidator])],
            lastName: ['', Validators.compose([Validators.required, AlphaValidator])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
            confirm: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        }, { validator: MatchingFieldValidator('password', 'confirm') });
    }

    logout() { this._authService.logout(); }

    closeModal(key: string) {
        jQuery(`#${key}`).foundation('close');
    }

    deleteUser() {
        this._userService.deleteUser(this.currentUser)
            .subscribe(resp => {
                window.location.href = '/api/users/logout';
            });
    }

    handleSubmission() {
        this._userService.updateUser(this.model)
            .subscribe(user => {
                this.currentUser = user;
                this.model = new User(user);
                this.closeModal('userDialog');
            },
            error => this.errorMessage = error.message);
    }

    ngOnInit() {
        this.buildForm();
        jQuery('user-panel').foundation();
        let uid = this._authService.getAuthedUser();
        this._userService.getUser(uid)
            .subscribe(user => {
                this.currentUser = user;
                this.model = new User(user);
            });
    }
}