import { Component, OnInit, EventEmitter } from 'angular2/core';
import { AccountService } from '../services/account.service';
import {Account} from '../model/account';

declare var jQuery: any;

@Component({
    templateUrl: '/view/account-panel.html',
    selector: 'account-panel',
    outputs: ['onAccountSelected'],
    providers: [AccountService]
})

export class AccountComponent implements OnInit {
    model: Account;
    accounts: Account[];
    errorMessage: String;
    dialogTitle: String;
    editingAccount: Boolean;
    onAccountSelected: EventEmitter<Account>;

    constructor(private _accountService: AccountService) {
        this.onAccountSelected = new EventEmitter<Account>();
    }

    ngOnInit() {
        jQuery('account-panel').foundation();
        this._accountService.getAccounts()
            .subscribe(accts => this.accounts = accts,
            error => this.errorMessage = error.message);
    }

    setModel(account?: Account) {
        if (account) {
            this.model = this._accountService.createAccount(account);
            this.dialogTitle = "Update Account";
            this.editingAccount = true;
        }
        else {
            this.model = this._accountService.createAccount();
            this.dialogTitle = "New Account";
            this.editingAccount = false;
        }
    }

    closeModal() {
        jQuery('#acctDialog').foundation('close');
    }

    deleteAccount() {
        this._accountService.deleteAccount(this.model)
            .subscribe(body => {
                let aid = body.aid;
                for (let account of this.accounts) {
                    if (account._id === aid) {
                        let acctIndex = this.accounts.indexOf(account);
                        this.accounts.splice(acctIndex, 1);
                        break;
                    }
                }
                this.closeModal();
            },
            error => this.errorMessage = error.message);
    }

    handleSubmission() {
        let id = this.model._id;
        if (id) {
            this._accountService.updateAccount(this.model)
                .subscribe(acct => {
                    for (let account of this.accounts) {
                        if (account._id === acct._id) {
                            let accountIndex = this.accounts.indexOf(account);
                            this.accounts.splice(accountIndex, 1, acct);
                            break;
                        }
                    }
                    this.closeModal();
                },
                error => this.errorMessage = error.message);
        }
        else {
            this._accountService.addAccount(this.model)
                .subscribe(account => {
                    this.accounts.push(account);
                    this.closeModal();
                },
                error => this.errorMessage = error.message);
        }
    }
    setSelected(account: Account) {
        this.onAccountSelected.emit(account);
        jQuery('#offCanvasLeft').foundation('close');
    }
}