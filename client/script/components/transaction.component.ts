/// <reference path="../../../typings/main/definitions/moment/index.d.ts" />
import * as moment from 'moment';

import { Component, OnInit, OnChanges, SimpleChange } from 'angular2/core';
import { FORM_DIRECTIVES, FormBuilder, Control, ControlGroup, Validators } from 'angular2/common';
import { Account } from '../model/account';
import { Transaction } from '../model/transaction';
import { TransactionService } from '../services/transaction.service';
import { AccountService } from '../services/account.service';
import { DateValidator } from './validators';
import { Category } from '../model/category';
import { CategoryService } from '../services/category.service';

declare var jQuery: any;

@Component({
    selector: 'transaction-panel',
    templateUrl: '/view/transaction-panel.html',
    inputs: ['account'],
    directives: [FORM_DIRECTIVES],
    providers: [TransactionService, AccountService, CategoryService]
})

export class TransactionPanelComponent implements OnInit, OnChanges {
    account: Account;
    model: Transaction;
    transactions: Transaction[] = [];
    title: String;
    balance: Number;
    editing: Boolean;
    transactionForm: ControlGroup;
    categories: Category[];

    constructor(
        private _transactionService: TransactionService,
        private _accountService: AccountService,
        private _formBuilder: FormBuilder,
        private _categoryService: CategoryService) { }

    ngOnInit() {
        this.setupForm();
        jQuery('#dateField').fdatepicker({
            initialDate: new Date(),
            format: 'mm/dd/yyyy',
            disableDblClickSelectoin: true,
            onRender: date => {
                return date.valueOf() >= Date.now() ? 'disabled' : '';
            }
        });
        jQuery('transaction-panel').foundation();
    }

    private setupCategories() {
        this._categoryService.getCategories()
            .subscribe(categories => {
                let typeControl = this.transactionForm.controls['type'];
                if (categories.length === 0) {
                    typeControl.setErrors({ noCategories: true });
                }
                this.categories = categories;
            });
    }

    setupForm() {
        this.model = this._transactionService.createTransaction();
        this.transactionForm = this._formBuilder.group({
            recipient: ['', Validators.required],
            date: ['', Validators.compose([Validators.required, DateValidator])],
            amount: ['', Validators.required],
            notes: [''],
            type: ['', Validators.required]
        });

        let dateControl = <Control>this.transactionForm.find('date');
        jQuery('#dateField').on('changeDate', evt => {
            // Pad the value. It's wrong for some reason
            let correctedDate = moment(evt.date);
            correctedDate.add(1, 'd');
            dateControl.updateValue(correctedDate.toDate(), { emitEvent: true });
        });

        this.setupCategories();
    }

    // Sets the dates in reverse chronological order
    sortTransactions() {
        this.transactions.sort((a, b) => {
            return moment(a.date).diff(moment(b.date));
        }).reverse();
    }

    getBalanceCurrency(balance: number) {
        return accounting.formatMoney(balance, '$', 2);
    }

    getDate(date: Date) {
        return moment(date).format('L');
    }

    private _updateBalance() {
        this._accountService.getBalance(this.account)
            .subscribe(balance => this.balance = balance);
    }

    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        if (changes['account']) {
            let account = changes['account'].currentValue;
            this._transactionService.getTransactions(account._id)
                .subscribe(transactions => {
                    this.transactions = transactions;
                    this.sortTransactions();
                });
            this._updateBalance();
        }
    }

    closeModal() {
        jQuery('#transactionDialog').foundation('close');
    }

    setModel(transaction?: Transaction) {
        this.setupCategories();
        if (transaction) {
            this.title = "Edit Transaction";
            this.editing = true;
            this.model = this._transactionService.createTransaction(transaction);
        }
        else {
            this.title = "New Transaction";
            this.editing = false;
            this.model = this._transactionService.createTransaction();
        }
        let dateControl = <Control>this.transactionForm.find('date');
        dateControl.updateValue(moment(this.model.date).format('L'));
    }

    changeType(type: Category) {
        this.model.type = type;
    }

    handleDelete() {
        this._transactionService.deleteTransaction(this.account._id, this.model)
            .subscribe(body => {
                let tid = body.tid;
                this.transactions.forEach(transaction => {
                    if (transaction._id === tid) {
                        let transactionIndex = this.transactions.indexOf(transaction);
                        this.transactions.splice(transactionIndex, 1);
                        return;
                    }
                });
                this.sortTransactions();
                this.closeModal();
            });
    }

    handleSubmission() {
        let acctId = this.account._id;
        let dateControl = this.transactionForm.find('date');
        this.model.date = moment(dateControl.value).toDate();
        if (this.model._id) {
            this._transactionService.updateTransaction(acctId, this.model)
                .subscribe(transaction => {
                    this.transactions.forEach(trans => {
                        if (trans._id === transaction._id) {
                            let transactionIndex = this.transactions.indexOf(trans);
                            this.transactions.splice(transactionIndex, 1, transaction);
                        }
                    });
                    this.sortTransactions();
                    this._updateBalance();
                    this.closeModal();
                });
        } else {
            this._transactionService.addTransaction(acctId, this.model)
                .subscribe(transaction => {
                    this.transactions.push(transaction);
                    this.sortTransactions();
                    this._updateBalance();
                    this.closeModal();
                });
        }
    }
}