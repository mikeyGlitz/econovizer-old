import {Component, OnInit} from "angular2/core";
import { FORM_DIRECTIVES, FormBuilder, ControlGroup, Control, Validators } from 'angular2/common';
import { AlphaValidator, MatchingFieldValidator, EmailValidator } from './validators';
import { UserService, AuthService } from '../services/user.service';
import {User} from '../model/user';

@Component({
    templateUrl: "/view/login.html",
    directives: [FORM_DIRECTIVES],
    providers: [AuthService]
})

export class LoginComponent implements OnInit {
    errorMessage: String;
    title = "Sign On";
    register = false;
    model: User;
    loginModel: ControlGroup;

    constructor(private _userService: UserService, private _authService: AuthService, private _formBuilder: FormBuilder) {
        this.buildLoginForm();
    }

    private buildLoginForm() {
        this.loginModel = this._formBuilder.group({
            email: ['', Validators.compose([Validators.required, EmailValidator])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        });
    }

    private buildSignUpForm() {
        this.loginModel = this._formBuilder.group({
            email: ['', Validators.compose([Validators.required, EmailValidator])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
            firstName: ['', Validators.compose([AlphaValidator, Validators.required])],
            lastName: ['', Validators.compose([AlphaValidator, Validators.required])],
            confirm: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        }, { validator: MatchingFieldValidator('password', 'confirm') });
    }

    toggleRegistration() {
        this.register = !this.register;

        if (this.register) {
            this.buildSignUpForm();
            this.title = "Register";
        }
        else {
            this.buildLoginForm();
            this.title = "Sign On"
        }
    }

    submitUser() {
        this._authService.authenticateOrRegister(this.model, this.register);
        this._authService.errorObservable.subscribe(error => {
            if (error) {
                this.errorMessage = error.message;
            }
        });
    }

    ngOnInit() {
        // We want to navigate away from this page if the user is already authenticated
        this._authService.checkAuthenticated();

        this.model = this._userService.createUser();
    }
}