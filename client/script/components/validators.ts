/// <reference path="../../../typings/main/definitions/moment/index.d.ts" />
import * as moment from 'moment';
import { Control, ControlGroup } from 'angular2/common';

export function DateValidator(dateControl: Control) {
    if (!moment(dateControl.value).isValid()) {
        return { dateError: true };
    }
}

export function AlphaValidator(control: Control) {
    let isAlphabetical = /^[a-zA-Z]+\s*[a-zA-Z]*$/.test(control.value);
    if (!isAlphabetical) {
        return { alpha: true };
    }
}

export function EmailValidator(control: Control) {
    let emailExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isEmail = emailExp.test(control.value);
    if (!isEmail) {
        return { email: true };
    }
}

export function MatchingFieldValidator(field1Key: string, field2Key: string) {
    return (group: ControlGroup) => {
        let control1 = group.controls[field1Key];
        let control2 = group.controls[field2Key];
        let control1Val = control1.value;
        let control2val = control2.value;
        if (control1Val !== control2val) {
            return control2.setErrors({ mismatch: true });
        }
    };
}