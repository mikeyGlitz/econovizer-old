import {Component, OnInit} from 'angular2/core';
import {ProtectedDirective} from '../directives/protected.directive';
import { UserPanelComponent } from './user.panel.component';

import { TransactionService } from '../services/transaction.service';
import { TransactionPanelComponent } from './transaction.component';

import { AccountService } from '../services/account.service';
import { Account } from '../model/account';
import { AccountComponent } from './account.component';

// This script will require that jQuery get loaded before it in the html page that uses it.
declare var jQuery: any;

@Component({
    templateUrl: '/view/dash.html',
    directives: [
        ProtectedDirective,
        UserPanelComponent,
        AccountComponent,
        TransactionPanelComponent
    ],
    providers: [AccountService, TransactionService]
})

export class DashComponent implements OnInit {
    selectedAccount: Account;
    ngOnInit() {
        jQuery('#content').foundation();
    }

    accountSelected(account: Account) {
        console.info('Selected:\n' + JSON.stringify(account));
        this.selectedAccount = account;
    }
}