import { Component, OnInit } from 'angular2/core';
import { FORM_DIRECTIVES, FormBuilder, ControlGroup, Control, Validators } from 'angular2/common';
import { CategoryService } from '../services/category.service';
import { Category } from '../model/category';
import { AlphaValidator } from './validators';

declare let jQuery: any;

@Component({
    selector: 'category-dialog',
    templateUrl: '/view/category-dialog.html',
    directives: [FORM_DIRECTIVES],
    providers: [CategoryService]
})

export class CategoryDialogComponent implements OnInit {
    categoryForm: ControlGroup;
    model: Category = this._categoryService.createCategory();
    errorMsg: String;
    categories: Category[];
    title: String;

    constructor(private _categoryService: CategoryService, private formBuilder: FormBuilder) { }

    buildForm() {
        this.categoryForm = this.formBuilder.group({
            name: ['', Validators.compose([Validators.required, AlphaValidator])],
            type: ['', Validators.required]
        });
    }

    ngOnInit() {
        this._categoryService.getCategories()
            .subscribe(categories => this.categories = categories);
        this.buildForm();
    }

    setModel(category?: Category) {
        this.model = this._categoryService.createCategory(category);
    }

    closeModal() {
        jQuery('#categoryDialog').foundation('close');
    }

    handleDelete() {
        this._categoryService.deleteCategory(this.model)
            .subscribe(body => {
                let cid = body.cid;
                for (let i = 0; i < this.categories.length; i++) {
                    let category = this.categories[i];
                    if (category._id === cid) {
                        this.categories.splice(i, 1);
                        this.closeModal();
                        return;
                    }
                }
            },
            error => this.errorMsg = error.message);
    }

    changeType(value: string) {
        this.model.type = value;
    }

    handleSubmission() {
        if (this.model._id) {
            this.title = "Edit Category";
            this._categoryService.updateCategory(this.model)
                .subscribe(category => {
                    this.categories.forEach(cat => {
                        if (cat._id === category._id) {
                            let categoryIndex = this.categories.indexOf(cat);
                            this.categories.splice(categoryIndex, 1, category);
                            this.closeModal();
                            return;
                        }
                    });
                },
                error => this.errorMsg = error.message);
        }
        else {
            this.title = "Create Category";
            this._categoryService.addCategory(this.model)
                .subscribe(category => {
                    this.categories.push(category);
                    this.closeModal();
                },
                err => this.errorMsg = err.message);
        }
    }
}