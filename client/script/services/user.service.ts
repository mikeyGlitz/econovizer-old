import { Injectable } from 'angular2/core';
import { Http, Headers, Response } from 'angular2/http';
import { Router, Location } from 'angular2/router';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/Rx';
import { User } from '../model/user';
import { ErrorReportable } from '../error-reportable';

@Injectable()
export class AuthService extends ErrorReportable {
    private errorObserver: Observer<any>;
    private error: any;
    errorObservable: Observable<any>;

    constructor(private _http: Http, private _router: Router, private _location: Location) {
        super();
        this.errorObservable = new Observable(observer => this.errorObserver = observer)
            .startWith(this.error)
            .share();
    }

    getAuthedUser() {
        return localStorage.getItem('uid');
    }

    logout() {
        localStorage.clear();
        sessionStorage.clear();
        window.location.href = `/api/users/logout?date=${Date.now()}`;
    }

    checkAuthenticated() {
        return this._http.get(`/api/users/loggedin?date=${Date.now()}`)
            .catch(error => this.reportError(error))
            .subscribe(resp => {
                if (resp.ok) {
                    let body = resp.json();
                    let uid = body.uid;
                    if (uid) {
                        localStorage.setItem('uid', uid);
                    }
                    this._router.navigate(['Dash']);
                }
            },
            error => {
                this._location.replaceState('/');
                this._router.navigate(['SignOn']);
            });
    }

    authenticateOrRegister(user: User, register: Boolean) {
        let uri = "/api/users/login";
        if (register) {
            uri = "/api/users";
        }

        return this._http.post(uri, user.toJSON(), {
            headers: new Headers({
                "Content-Type": "application/json"
            })
        })
            .map(resp => resp.json())
            .catch(error => this.reportError(error))
            .subscribe(body => {
                let uid = body.uid;
                if (uid) {
                    localStorage.setItem('uid', uid);
                }
                this._router.navigate(['Dash']);
            },
            error => {
                console.error(error);
                this.error = error;
                this.errorObserver.next(error);
            });
    }
}

@Injectable()
export class UserService extends ErrorReportable {
    currentUid: String;
    constructor(private _http: Http) { super(); }

    createUser(user?: User) {
        if (user) {
            return new User(user);
        }
        return new User();
    }

    getUser(uid: String) {
        return this._http.get(`/api/users/${uid}?date=${Date.now()}`)
            .map(resp => <User>resp.json())
            .catch(error => this.reportError(error, "unable to fetch user"));
    }

    deleteUser(user: User) {
        let uid = user._id;
        return this._http.delete(`/api/users/${uid}`)
            .map(resp => resp.json())
            .catch(error => this.reportError(error, "Unable to delete user"));
    }

    updateUser(user: User) {
        let uid = user._id;
        return this._http.put(`/api/users/${uid}`, user.toJSON(), {
            headers: new Headers({
                "Content-Type": "application/json"
            })
        })
            .map(resp => <User>resp.json())
            .catch(error => this.reportError(error, "Unable to update user"));
    }
}