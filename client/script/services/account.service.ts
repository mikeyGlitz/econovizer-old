import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import {Account} from '../model/account';
import { Observable } from 'rxjs/Observable';
import { ErrorReportable } from '../error-reportable';
import 'rxjs/Rx';

@Injectable()
export class AccountService extends ErrorReportable {
    private _uid = localStorage.getItem('uid');
    selectedAccount: Account;

    constructor(private _http: Http) {
        super();
    }

    createAccount(account?: Account) {
        return new Account(account);
    }

    getAccounts() {
        return this._http.get(`/api/users/${this._uid}/accounts?date=${Date.now()}`, {
            headers: new Headers({
                "Cache-Control": "max-age=0,no-cache,no-store"
            })
        })
            .map(resp => <Account[]>resp.json())
            .catch(error => {
                console.error(error);
                return Observable.throw(error.json() || "Unable to get accounts");
            });
    }

    getBalance(account: Account) {
        return this._http.get(`/api/users/${this._uid}/accounts/${account._id}/balance?date=${Date.now()}`)
            .map(resp => resp.json().balance)
            .catch(error => this.reportError(error));
    }

    addAccount(account: Account) {
        return this._http.post(`/api/users/${this._uid}/accounts`, account.toJSON(), {
            headers: new Headers({
                "Content-Type": "application/json"
            })
        })
            .map(resp => <Account>resp.json())
            .catch(error => this.reportError(error));
    }

    deleteAccount(account: Account) {
        let uri = `/api/users/${this._uid}/accounts/${account._id}`;
        return this._http.delete(uri)
            .map(resp => resp.json())
            .catch(error => this.reportError(error))
    }

    updateAccount(account: Account): Observable<Account> {
        let uri = `/api/users/${this._uid}/accounts/${account._id}`;
        let respBody = account.toJSON();
        return this._http.put(uri, respBody, {
            headers: new Headers({
                "Content-Type": "application/json"
            })
        })
            .map(resp => <Account>resp.json())
            .catch(error => this.reportError(error));
    }
}