import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import { Transaction } from '../model/transaction';
import { ErrorReportable } from '../error-reportable';
import 'rxjs/Rx';

@Injectable()
export class TransactionService extends ErrorReportable {
    uid: String = localStorage.getItem('uid');

    constructor(private _http: Http) {
        super();
    }

    createTransaction(transaction?: Transaction) {
        return new Transaction(transaction);
    }

    getTransactions(acctId: String) {
        let url = `/api/users/${this.uid}/accounts/${acctId}/transactions?date=${Date.now()}`;
        return this._http.get(url)
            .map(resp => <Transaction[]>resp.json())
            .catch(error => this.reportError(error));
    }

    addTransaction(acctId: String, transaction: Transaction) {
        let url = `/api/users/${this.uid}/accounts/${acctId}/transactions`;
        return this._http.post(url, transaction.toJSON(), {
            headers: new Headers({
                "Content-Type": "application/json"
            })
        })
            .map(resp => <Transaction>resp.json())
            .catch(error => this.reportError(error));
    }

    updateTransaction(acctId: String, transaction: Transaction) {
        let transId = transaction._id;
        let url = `/api/users/${this.uid}/accounts/${acctId}/transactions/${transId}`;

        return this._http.put(url, transaction.toJSON(), {
            headers: new Headers({
                "Content-Type": "application/json"
            })
        })
            .map(resp => <Transaction>resp.json())
            .catch(error => this.reportError(error));
    }

    deleteTransaction(acctId: String, transaction: Transaction) {
        let transId = transaction._id;
        let url = `/api/users/${this.uid}/accounts/${acctId}/transactions/${transId}`;

        return this._http.delete(url)
            .map(resp => resp.json())
            .catch(error => this.reportError(error));
    }
}