import { Injectable } from 'angular2/core';
import { Http, Headers } from 'angular2/http';
import { Category } from '../model/category';
import 'rxjs/Rx';
import { ErrorReportable } from '../error-reportable';

@Injectable()
export class CategoryService extends ErrorReportable {
    private uid = localStorage.getItem('uid');
    constructor(private _http: Http) {
        super();
    }

    createCategory(category?: Category) {
        return new Category(category);
    }

    getCategories() {

        return this._http.get(`/api/users/${this.uid}/categories?date=${Date.now()}`)
            .map(resp => <Category[]>resp.json())
            .catch(error => this.reportError(error));
    }

    addCategory(category: Category) {
        return this._http.post(`/api/users/${this.uid}/categories`, category.toJSON(), {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
            .map(resp => <Category>resp.json())
            .catch(error => this.reportError(error));
    }

    updateCategory(category: Category) {
        return this._http.put(`/api/users/${this.uid}/categories/${category._id}`, category.toJSON(), {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
            .map(resp => <Category>resp.json())
            .catch(error => this.reportError(error));
    }

    deleteCategory(category: Category) {
        return this._http.delete(`/api/users/${this.uid}/categories/${category._id}`)
            .map(resp => resp.json())
            .catch(error => this.reportError(error));
    }
}